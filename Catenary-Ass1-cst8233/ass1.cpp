/****************************************************
* Student Name: Brandon Gould
* Student Number: 040 872 459
* Assignment Number: 1
* Course Name & Number: Numerical Computing CST8233
* Lab Section: 303
* Professor's Name: Andrew Tyler
* Due Date: 2018-10-14
* Submission Date: 2018-10-14
* Files: ass1.cpp
****************************************************/

#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>

using namespace std;

void canteraryMaclaurin();

/*****************************************************
Function Name: main
Purpose: Main Menu of program, and calls to needed functions based on user input.
In paramaters: none
Out paramaters: 0 on success
Version: 1.0.0
Author: Brandon Gould
*****************************************************/
int main(void) {
	char option; /* UserInput for menu option */
	bool running = true; /* Loop for user choice */

	/* Main Menu Options */
	while (running) {
		cout << "******************************" << endl; /* Border */
		cout << "1. Evaulate the function" << endl << "2. Quit" << endl; /* Menu Options */
		cout << "******************************" << endl; /* Border */
		cin >> option; /* UserInput for menu */

		switch (option) {
		case '1':
			canteraryMaclaurin();
			break;
		case '2':
			running = false;
			cout << "quit" << endl;
			break;
		default:
			cout << "Choose a valid option." << endl;
		}
		cout << endl; /* Spacer: End of current rotation */
	}
	return 0;
}

/*****************************************************
Function Name: canteraryMaclaurin
Purpose: Evaluate a Canterary Function through Maclaurin series. User input for values. Compares faster but more inaccurate measurement versus math library.
In parameters: none
Out parameters: none
Version: 1.0.0
Author: Brandon Gould
*****************************************************/
void canteraryMaclaurin() {
	short maxPower = 20; /* Starting at higher than valid */
	double lowRange = 20, highRange = 20, scaleFactor = 20; /* Starting at higher than valid */

	cout << endl << "        EVALUATING THE CATENARY SERIES APPROXIMATION" << endl << endl;

	/* User Inputs for needed values */
	while (maxPower != 0 && maxPower != 2 && maxPower != 4 && maxPower != 6 && maxPower != 8 && maxPower != 10){
		cout << "Please enter the highest power of x in the catenary series (0, 2, 4, 6, 8, or 10): ";
		cin >> maxPower;
	}
	cout << endl << "        CHOOSE THE RANGE OF EVALUATION - low x to high x" << endl;
	while (lowRange < -10 || lowRange > 0) {
		cout << "Please enter low x - in the range -10.0 to 0.0: ";
		cin >> lowRange;
	}
	while (highRange > 10 || highRange < 0) {
		cout << "Please enter high x - in the range 0.0 to +10.0: ";
		cin >> highRange;
	}
	while (scaleFactor > 10 || scaleFactor < 0) {
		cout << endl << "Please enter scale factor the range 0.0 to +10.0: ";
		cin >> scaleFactor;
	}

	cout << endl << endl << "CATENARY SERIES TO x^" << maxPower << " from x - " << lowRange << " to x - " << highRange << endl;
	/*** VALUES ***/
	/* xVal for x value. incFactor for X value increase. scaleFactor for A value. maxPower for exponent to evaluate until. zVal is (x/a). */
	/* Series for the resulting value of the series. Exact is the math function evaluation. eError is the Exact Error %. tError is the Truncation Error %. */
	double series, zVal, incFactor, xVal, exact, eError, tError,trunc;
	incFactor = (highRange + (lowRange * -1)) / 10; /* Increment Factor for the X value. High + Absolute of Low (always 0 or negative), then divide all by 10 for increments */
	xVal = lowRange; /* Set initial X value at lowest point first */

	/* Print Header */
	cout << setiosflags(ios::left);
	cout << setw(15) << "x" << setw(17) << "Series" << setw(15) << "Exact" << setw(15) << "Exact % Error" << setw(15) << "Trunc. % Error" << endl;

	/* Math */
	for (short i = 0; i <= 10; i++) { /* 0-10 increments of x */
		series = 0; /* (Re)Set series value to 0. */
		zVal = xVal / scaleFactor; /* Set correct Z value */
		if (0 <= maxPower) { /* x^0 */
			series += ( scaleFactor * 1 );
			trunc = (scaleFactor * (zVal*zVal) / 2); /* 2! = 2 */
		}
		if (2 <= maxPower) { /* x^2 */
			series += trunc; 
			trunc = (scaleFactor * (zVal*zVal*zVal*zVal) / 24.0); /* 4! = 24 */
		}
		if (4 <= maxPower) { /* x^4 */
			series += trunc;
			trunc = (scaleFactor * (zVal*zVal*zVal*zVal*zVal*zVal) / 720.0); /* 6! = 720 */
		}
		if (6 <= maxPower) { /* x^6 */
			series += trunc;
			trunc = (scaleFactor * (zVal*zVal*zVal*zVal*zVal*zVal*zVal*zVal) / 40320.0); /* 8! = 40320 */
		}
		if (8 <= maxPower) { /* x^8 */
			series += trunc;
			trunc = (scaleFactor * (zVal*zVal*zVal*zVal*zVal*zVal*zVal*zVal*zVal*zVal) / 3628800.0); /* 10! = 3628800 */
		}
		if (10 <= maxPower) { /* x^10 */
			series += trunc;
			trunc = (scaleFactor * (zVal*zVal*zVal*zVal*zVal*zVal*zVal*zVal*zVal*zVal*zVal*zVal) / 479001600.0); /* 12! = 479001600 */
		}
		exact = scaleFactor * cosh(zVal);
		eError = 100 * (exact - series) / exact;
		tError = 100 * trunc / series;

		/*** PRINT ***/
		cout << setiosflags(ios::scientific|ios::left|ios::showpos);
		cout << setw(10) << setprecision(3) << xVal << "     " << resetiosflags(ios::showpos) << setw(10) << setprecision(5) << series << "     " << setw(10) << setprecision(5) << exact << "     " << setw(10) << setprecision(5) << eError << "     " << setw(10) << setprecision(5) << tError << endl;

		xVal += incFactor; /* Increment xVal */
	}
}