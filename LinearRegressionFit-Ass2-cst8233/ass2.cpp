/***********************************************
Student name: 040 872 459
Student number: Brandon Gould
Assignment number: 2
Course name and number: CST8233 300
Lab section number: 303
Professorís name: Andrew Tyler
Due date of assignment: 2018-11-11
Submission date of assignment: 2018-11-11
List of source and header files in the project: ass2.cpp 
************************************************/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <iomanip> 

using namespace std;

void dataMenu(int type);

/*****************************************************
Function Name: main
Purpose: Main Menu of program, and calls to needed functions based on user input.
In paramaters: none
Out paramaters: 0 on success
Version: 1.0.0
Author: Brandon Gould
*****************************************************/
int main(void) {
	char option; /* UserInput for menu option */
	bool running = true; /* Loop for user choice */

						 /* Main Menu Options */
	while (running) {
		cout << "MENU" << endl;
		cout << "1. Linear Fit" << endl << "2. Power Law Fit" << endl << "3. Quit" << endl; /* Menu Options */
		cin >> option; /* UserInput for menu */

		switch (option) {
		case '1':
			dataMenu(1);
			break;
		case '2':
			dataMenu(2);
			break;
		case '3':
			running = false;
			cout << "quit" << endl;
			break;
		default:
			cout << "Choose a valid option." << endl;
		}
	}
	return 0;
}

/*****************************************************
Function Name: dataMenu
Purpose: Menu for interpolate and extrapolation. Calculates the Least Squares Linear Regression. Prints formula for Power Law or Linear. 
In paramaters: int type
Out paramaters: none
Version: 1.0.0
Author: Brandon Gould
*****************************************************/
void dataMenu(int type) {
	char option; /* UserInput for menu option */
	bool running = true; /* Loop for user choice */

	string fileName; //file name 
	vector<double> xvec; // vector to hold X values
	vector<double> yvec; // vector to hold Y values
	ifstream file; // file stream
	string line; // string to hold lines from file
	double d1, d2; // temp doubles for where needed
	double s = 0; // number of records


	double year; // year for extrapolation

	// open file from user input
	cout << "Please enter the file to open: ";
	cin >> fileName;
	file.open(fileName, ifstream::in);
	// read file line by line and push variables into x y vectors
	if (file.is_open()) {
		while (getline(file, line)) {
			istringstream iss(line);
			iss >> d1 >> d2;
			xvec.push_back(d1);
			yvec.push_back(d2);
		}
	}
	file.close();

	if (true) {
		// linear
		double sx = 0, sy = 0, sxy = 0, sxx = 0, delta = 0, m, c;
		// s = N
		s = xvec.size();
		if (type == 2) { // For Power Law log all x and y
			for (int i = 0; i < s; i++) {
				xvec[i] = log(xvec[i]); 
				yvec[i] = log(yvec[i]);
			}
		}
		// sx = sigma x ; xy = sigma y ; sxy = sigma x*y ; sxx = sigma x^2 ; delta = S * Sxx - (Sx^2)
		for (int i = 0; i < s; i++) {
			sx += xvec[i];
			sy += yvec[i];
			sxy += xvec[i] * yvec[i];
			sxx += xvec[i] * xvec[i];
		}
		delta = (s * sxx) - (sx*sx);
		//calculate m and c
		m = ((s * sxy) - (sx * sy)) / delta;
		c = ((sxx * sy) - (sx * sxy)) / delta;
		
		if (type == 1) {
			d1 = m; // set m to double with more scope
			d2 = c; // set c to double with more scope
		}
		else if (type == 2) {
			d1 = m; //b
			d2 = exp(c); //c
		}
	}
	
	//print records
	cout << "There are " << s << " records." << endl;
	// print equation
	if (type == 1) {
		cout << fixed << setprecision(2) << "y = " << d1 << "*x + " << d2 << endl;
	}
	else {
		cout << fixed << setprecision(2) << "y = " << d2 << "x^" << d1 << endl;
	}
	

						 /* Data Menu Options */
	while (running) {
		cout << "MENU" << endl;
		cout << "1. Interpolate/Extrapolate" << endl << "2. Main Menu" << endl; /* Menu Options */
		cin >> option; /* UserInput for menu */

		switch (option) {
		case '1':
			cout << "Please enter the year to interpolate/extrapolate to: ";
			cin >> year;

			if (type == 1) { //linear
				cout << "The linear interpolated/extrapolated CO2 level in year " << setprecision(1) << year << " is " << setprecision(2) << (316.5+(d1*(year - 1960) + d2))  << endl;
			}
			else { // power law
				// power
				cout << "The power law interpolated/extrapolated CO2 level in year " << setprecision(1) << year << " is " << setprecision(2) << 316.5+(d2 * pow((year - 1960), d1)) << endl;
			}
			break;
		case '2':
			running = false; // could just return instead, this way allows clean up or closing options if ever needed
			break;
		default:
			cout << "Choose a valid option." << endl;
		}
	}
}

