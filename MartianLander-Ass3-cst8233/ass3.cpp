/****************************************************
* Student Name: Brandon Gould
* Student Number: 040 872 459
* Assignment Number: 3
* Course Name & Number: Numerical Computing CST8233
* Lab Section: 303
* Professor's Name: Andrew Tyler
* Due Date: 2018-12-02
* Submission Date: 2018-12-02
* Files: ass3.cpp
****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cmath>
#include <Windows.h>
#include <vector>
#include <conio.h>

using namespace std;

/*
Struct to hold the high score data, and file name.
*/
struct data {
	vector<string> names;
	vector<int> bounces;
	vector<double> times;
	string fileName;
} saveData;

void MartianLander(struct data &saveData);
void loadFile(struct data &saveData);

/*****************************************************
Function Name: main
Purpose: Open File & Main Menu
In paramaters: none
Out paramaters: 0 on success
Version: 1.0.0
Author: Brandon Gould
*****************************************************/
int main(void) {
	char option; /* UserInput for menu option */
	bool running = true; /* Loop for user choice */

	cout << "******************************" << endl << "Martian Lander" << endl << "******************************" << endl;
	cout << "Enter save file name: ";
	cin >> saveData.fileName;
	loadFile(saveData);

						 /* Main Menu Options */
	while (running) {
		cout << "******************************" << endl; /* Border */
		cout << "1. Run Simulation" << endl << "2. Quit" << endl; /* Menu Options */
		cout << "******************************" << endl; /* Border */
		cin >> option; /* UserInput for menu */

		switch (option) {
		case '1':
			MartianLander(saveData);
			break;
		case '2':
			running = false;
			cout << "quit" << endl;
			break;
		default:
			cout << "Choose a valid option." << endl;
		}
		cout << endl; /* Spacer: End of current rotation */
	}
	return 0;
}

/*****************************************************
Function Name: MartianLander
Purpose: Everything. Calculate the descent of Martian Lander and all related variables, update screen. When finished save data.
In parameters: struct data &
Out parameters: none
Version: 1.0.0
Author: Brandon Gould
*****************************************************/
void MartianLander(struct data &saveData) {
	bool sim = true; /* Loop */
	string user;

	double g = 3.7;
	double c = 0.13;
	double a = 8.3;
	double vmax = 46;

	double height = 1000;
	double burn = 0;
	double deltaT = 0;
	double reserve = 100;
	int bounce = 0;
	double v = 0;
	double vP = 0;
	
	double start = GetTickCount();
	double oldTime = start;
	double thisTime = start;
	double totalTime = 0;

	int minute = 0;
	int second = 0;
	int millisecond = 0;
	int nMinute = 0;
	int nSecond = 0;
	int nMillisecond = 0;

	string state = "Starting Descent";

	while (sim) {
		//check key press
		if (kbhit()) {
			char ch = getch();
			if (ch == 'w') {
				if (reserve > 0)
					burn++;
				state = "W was pressed - increase burn";
			}
			else if (ch == 'e') {
				if (burn > 0) 
					burn--;
				state = "E was pressed - decrease burn";
			}
			
		}
		// Math - velocity
		deltaT = (thisTime - oldTime)/1000; //delta time, converted to seconds from milliseconds
		vP = v + (g - c * (v + a * pow(v / vmax, 3)) - burn) * deltaT;
		vP = (vP + v) / 2;
		v = v + (g - c * (vP + a * pow(vP / vmax, 3)) - burn) * deltaT;
		// update height
		height -= v*deltaT;

		// check if hit ground
		if (height < 1) {
			//check if bounce or stop
			if (v < 1) {
				sim = false; //landed
			}
			else { // bounce
				bounce++;
				v = v*(-1);
			}
		}
		reserve = reserve - (burn*deltaT);
		if (reserve <= 0) {
			burn = 0; reserve = 0;
		}
		minute = (int) (totalTime / 60000);
		second = (int) ((totalTime - (minute * 60000)) / 1000);
		millisecond = (int) ((totalTime - (minute * 60000) - (second * 1000)));
		//Print TODO: update/clear?
		system("cls");
		cout << "*************************" << state << "*************************" << endl;
		cout << ": BURN = " << burn << "; BURN RESERVE = " << reserve << endl;
		cout << "Minute = " << minute << "; SECOND = " << second << "; MILLISECOND = " << millisecond << endl;
		cout << "SPEED OF DESCENT = " << v << endl;
		cout << "HEIGHT ABOVE MARTIAN SURFACE = " << height << endl;
		cout << "NUMBER OF BOUNCES = " << bounce << endl;

		// Pause & Update Time
		oldTime = thisTime; // update old time
		thisTime = GetTickCount(); // update this time
		while (thisTime <= oldTime) { //make sure time is different
			Sleep(1);
			thisTime = GetTickCount();
		}
		totalTime = thisTime - start;

	}

	
	// save score + name
	cout << "Enter Name (no spaces): ";
	cin >> user;
	/* SORT */
	//if first score
	if (saveData.bounces.size() == 0) {
		saveData.names.push_back(user);
		saveData.bounces.push_back(bounce);
		saveData.times.push_back(totalTime);
	}
	else {
		bool added = false;
		vector<string> names;
		vector<int> bounces;
		vector<double> times;
		for (unsigned int i = 0; i < saveData.names.size(); i++) {
			// new is better than current
			if (!added && (saveData.bounces[i] > bounce || (saveData.bounces[i] == bounce && saveData.times[i] > totalTime))) {
				names.push_back(user);
				bounces.push_back(bounce);
				times.push_back(totalTime);
				added = true;
			}
			// always save current
			names.push_back(saveData.names[i]);
			bounces.push_back(saveData.bounces[i]);
			times.push_back(saveData.times[i]);
		}
		//last place rip
		if (!added) {
			names.push_back(user);
			bounces.push_back(bounce);
			times.push_back(totalTime);
		}
		//replace
		saveData.names = names;
		saveData.bounces = bounces;
		saveData.times = times;
	}
	

	//Print Scores
	cout << endl << "******************************" << endl << "High Scores" << endl << "******************************" << endl;
	for (unsigned int i = 0; i < saveData.names.size(); i++) {
		cout << saveData.names[i] << "  " << saveData.bounces[i] << "  " << saveData.times[i] << endl;
	}
	
	//save scores
	ofstream outFile;
	outFile.open(saveData.fileName);
	if (outFile.is_open()) {
		outFile << "Martin Lander Score File" << endl; //first line is title
		for (unsigned int i = 0; i < saveData.names.size(); i++) {
			outFile << saveData.names[i] << "  " << saveData.bounces[i] << "  " << saveData.times[i] << endl;
		}
		outFile.close();
	}
	else cout << "Unable to open.";

}

/*****************************************************
Function Name: loadFile
Purpose: From File save the high scores into vectors inside saveData struct
In parameters: struct data &
Out parameters: none
Version: 1.0.0
Author: Brandon Gould
*****************************************************/
void loadFile(struct data &saveData) {
	ifstream inFile;
	string tN;
	int tB;
	double tT;
	string line; // string to hold lines from file
	inFile.open(saveData.fileName, ifstream::in);
	// read file line by line and push variables into x y vectors
	if (inFile.is_open()) {
		getline(inFile, line); //first line is title, garbage
		while (getline(inFile, line)) {
			istringstream iss(line);
			iss >> tN >> tB >> tT;
			saveData.names.push_back(tN);
			saveData.bounces.push_back(tB);
			saveData.times.push_back(tT);
		}
	}
	inFile.close();
}